package jnhamilton.selenium.testdata;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

/**
 * Provides access to the data contained in the products.json file to be used in the tests
 */
@SuppressWarnings("unchecked")
public class LoadProduct {

    public JSONArray getProductJsonArray(String product) {
        var currentDir = System.getProperty("user.dir");
        var productsPath = "/src/main/resources/products.json";
        var productFilePath = currentDir + productsPath;

        JSONObject productJson;
        JSONArray productDetails = null;

        try {
            FileReader reader = new FileReader(productFilePath);
            JSONParser jsonParser = new JSONParser();
            productJson = (JSONObject) jsonParser.parse(reader);
            productDetails = (JSONArray) productJson.get(product);

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
        return productDetails;
    }

    public String getBookProductID(String aBook) {
        String productId = null;

        JSONArray bookDetails = getProductJsonArray(aBook);

        for (JSONObject bookObj : (Iterable<JSONObject>) bookDetails) {
            productId = (String) bookObj.get("productId");
        }
        return productId;
    }


    public String getBookTitle(String aBook) {
        String bookTitle = null;

        JSONArray bookDetails = getProductJsonArray(aBook);

        for (JSONObject bookObj : (Iterable<JSONObject>) bookDetails) {
            bookTitle = (String) bookObj.get("productTitle");
        }
        return bookTitle;
    }

    public String getBookEdition(String aBook) {
        String bookEdition = null;

        JSONArray bookDetails = getProductJsonArray(aBook);

        for (JSONObject bookObj : (Iterable<JSONObject>) bookDetails) {
            bookEdition = (String) bookObj.get("edition");
        }
        return bookEdition;

    }

    public String getBookAuthor(String abook) {
        String bookAuthor = null;

        JSONArray bookDetails = getProductJsonArray(abook);

        for (JSONObject bookObj : (Iterable<JSONObject>) bookDetails) {
            bookAuthor = (String) bookObj.get("author");
        }
        return bookAuthor;
    }
}