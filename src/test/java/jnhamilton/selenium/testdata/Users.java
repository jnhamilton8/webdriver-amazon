package jnhamilton.selenium.testdata;

public final class Users {

    public static final String VALID_USERNAME1 = "js8957183@gmail.com";

    public static final String INVALID_USERNAME1 = "test@test.com";
    public static final String INVALID_PASSWORD1 = "hiswillnotwork";
}
