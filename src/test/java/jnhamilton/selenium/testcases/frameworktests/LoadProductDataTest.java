package jnhamilton.selenium.testcases.frameworktests;

import jnhamilton.selenium.testdata.LoadProduct;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class LoadProductDataTest {

    @Test
    public void loadBookDataTest() {
        LoadProduct book = new LoadProduct();

        String book1ProductID = book.getBookProductID("book1");
        assertThat(book1ProductID, is("1937785025"));

        String book1ProductTitle = book.getBookTitle("book1");
        assertThat(book1ProductTitle, is("Explore It!: Reduce Risk and Increase Confidence with Exploratory Testing"));

        String book1ProductEdition = book.getBookEdition("book1");
        assertThat(book1ProductEdition, is("Paperback"));

        String book1ProductAuthor = book.getBookAuthor("book1");
        assertThat(book1ProductAuthor, is("Elisabeth Hendrickson"));
    }
}