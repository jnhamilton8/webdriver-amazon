package jnhamilton.selenium.testcases;

import jnhamilton.selenium.testdata.Users;
import jnhamilton.selenium.utils.PageObjectFactory;
import jnhamilton.selenium.utils.TestBase;
import org.testng.annotations.*;

import java.util.List;

import static jnhamilton.selenium.testdata.Users.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class LoginTest extends TestBase {

    private PageObjectFactory pageObjectFactory;

    @BeforeClass
    public void setUp() {
        pageObjectFactory = new PageObjectFactory(getDriver());
        pageObjectFactory.setupPageObjects(List.of("signInPage"));
    }

    @Test(priority = 1)
    public void aValidUserBringsUpPasswordResetMessage() {
        pageObjectFactory.signInPage().navigateDirectlyToSignInPage();
        pageObjectFactory.signInPage().enterUserName(VALID_USERNAME1);

        assertThat("The password reset message was not displayed", pageObjectFactory.signInPage().getTextOfNotificationOnLogIn(), is("Password reset required"));
    }

    @Test(priority = 2)
    public void anInvalidUserBringsUpAuthWarning() {
        pageObjectFactory.signInPage().navigateDirectlyToLoginPageAndLogIn(INVALID_USERNAME1, INVALID_PASSWORD1);

        assertThat("The auth warning was not displayed", pageObjectFactory.signInPage().getVisibilityOfAuthWarning(), is(true));
    }
}