package jnhamilton.selenium.testcases;

import jnhamilton.selenium.utils.PageObjectFactory;
import jnhamilton.selenium.utils.TestBase;
import org.testng.annotations.*;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BasketReviewTest extends TestBase {

    private PageObjectFactory pageObjectFactory;

    @BeforeClass
    public void setUp() {
        pageObjectFactory = new PageObjectFactory(getDriver());
        pageObjectFactory.setupPageObjects(List.of("homePage", "basketReviewPage", "productPage", "shoppingBasketPage"));
    }

    @Test
    public void addToBasketAndVerifyExpectedSubTotalPriceOnBasketReviewPage() throws InterruptedException {
        pageObjectFactory.homePage().navigateToHomePage();
        pageObjectFactory.shoppingBasketPage().removeAllItemsFromBasketIfPresent();

        pageObjectFactory.productPage().navigateToBookProductPage("book1");
        var expectedSubTotalPrice = pageObjectFactory.productPage().getPrice();

        pageObjectFactory.productPage().addItemToBasket("book1");

        pageObjectFactory.basketReviewPage().verifyOnBasketReviewPage();
        var actualBasketSubtotalPrice = pageObjectFactory.basketReviewPage().getCartSubTotal();

        assertThat("The price on the Basket Review Page did not match the expected price", actualBasketSubtotalPrice,
                equalTo(expectedSubTotalPrice));
    }
}