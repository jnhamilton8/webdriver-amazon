package jnhamilton.selenium.testcases;

import jnhamilton.selenium.testdata.LoadProduct;
import jnhamilton.selenium.utils.PageObjectFactory;
import jnhamilton.selenium.utils.TestBase;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;

public class BasketTest extends TestBase {

    private PageObjectFactory pageObjectFactory;

    @BeforeClass
    public void setUp() {
        pageObjectFactory = new PageObjectFactory(getDriver());
        pageObjectFactory.setupPageObjects(List.of("homePage", "productPage", "topBanner", "shoppingBasketPage"));
    }

    @BeforeMethod
    public void beforeMethod() throws InterruptedException {
        pageObjectFactory.homePage().navigateToHomePage();
        pageObjectFactory.shoppingBasketPage().removeAllItemsFromBasketIfPresent();
    }


    @Test(priority = 1)
    public void canAddItemsToBasket() {
        pageObjectFactory.productPage().addItemToBasket("book1");
        pageObjectFactory.productPage().addItemToBasket("book2");

        assertThat("The number of items in the basket did not match that expected", pageObjectFactory.topBanner().getNumberOfItemsListedInShoppingCartIcon(), is("2"));

        pageObjectFactory.topBanner().selectShoppingCartIcon();
        ArrayList<String> basketItems = pageObjectFactory.shoppingBasketPage().getHeadersOfAllItemsInBasket();

        assertThat("The items in the Basket did not match those expected", basketItems, hasItems(new LoadProduct().getBookTitle("book1"),
                new LoadProduct().getBookTitle("book2")));
    }

    @Test(priority = 2, dependsOnMethods = "canAddItemsToBasket")
    public void canDeleteItemsFromBasket() throws InterruptedException {
        pageObjectFactory.productPage().addItemToBasket("book1");
        pageObjectFactory.productPage().addItemToBasket("book2");

        pageObjectFactory.shoppingBasketPage().removeAllItemsFromBasketIfPresent();
        pageObjectFactory.topBanner().selectShoppingCartIcon();

        assertThat("The basket was not empty as expected", pageObjectFactory.topBanner().getNumberOfItemsListedInShoppingCartIcon(), is("0"));
        assertThat("The shopping basket message wasn't as expected", pageObjectFactory.shoppingBasketPage().getShoppingCartMessage(), is("Your Amazon basket is empty"));
    }
}