package jnhamilton.selenium.testcases;

import jnhamilton.selenium.testdata.LoadProduct;
import jnhamilton.selenium.utils.PageObjectFactory;
import jnhamilton.selenium.utils.TestBase;
import jnhamilton.selenium.utils.Urls;
import org.testng.annotations.*;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;

public class NavigationTest extends TestBase {

    private PageObjectFactory pageObjectFactory;

    @BeforeClass
    public void setUp() {
        pageObjectFactory = new PageObjectFactory(getDriver());
        pageObjectFactory.setupPageObjects(List.of("homePage", "productPage", "shoppingBasketPage", "signInPage", "topBanner"));
    }

    @Test
    public void testUserCanNavigateDirectlyToTheHomePage() {
        pageObjectFactory.homePage().navigateToHomePage();

        assertThat("The user was not taken to the HomePage", pageObjectFactory.commonUtils().getCurrentURL(), is(Urls.BASE_URL.getUrl()));
    }

    @Test
    public void testUserCanNavigateDirectlyToTheProductPage() {
        LoadProduct products = new LoadProduct();

        pageObjectFactory.productPage().navigateToBookProductPage("book1");

        assertThat("The user was not taken to the Product Page", pageObjectFactory.commonUtils().getCurrentURL(), containsString(
                Urls.PRODUCTS_URL.getUrl() + products.getBookProductID("book1")));
    }

    @Test
    public void testUserCanNavigateDirectlyToTheShoppingBasketPage() {
        pageObjectFactory.shoppingBasketPage().navigateToShoppingBasketPage();

        assertThat("The user was not taken to the Shopping Basket Page", pageObjectFactory.commonUtils().getCurrentURL(), is(Urls.SHOPPING_BASKET_PAGE.getUrl()));
    }

    @Test
    public void testUserCanNavigateDirectlyToTheSignInPage() {
        pageObjectFactory.signInPage().navigateDirectlyToSignInPage();

        assertThat("The user was not taken to the SignIn Page", pageObjectFactory.commonUtils().getCurrentURL(), is(Urls.SIGN_IN_PAGE.getUrl()));
    }

    @Test
    public void testUserCanNavigateToTheSignInPageViaTheTopBanner() {
        pageObjectFactory.homePage().navigateToHomePage();
        pageObjectFactory.topBanner().navigateToSignInPageFromTopBanner();

        assertThat("The user was not taken to the SignIn Page", pageObjectFactory.commonUtils().getCurrentURL(), containsString("signin"));
    }
}