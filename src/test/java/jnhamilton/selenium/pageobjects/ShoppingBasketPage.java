package jnhamilton.selenium.pageobjects;

import jnhamilton.selenium.utils.CommonUtils;
import jnhamilton.selenium.utils.Urls;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ShoppingBasketPage {

    private final CommonUtils commonUtils;

    public ShoppingBasketPage(CommonUtils commonUtils) {
        this.commonUtils = commonUtils;
    }

    private final By SHOPPING_BASKET_MESSAGE = By.xpath("//div[@class = 'a-row sc-your-amazon-cart-is-empty']/h2");
    private final By HEADER_OF_ITEM_IN_BASKET = By.xpath("//span[contains(@class,'product-title')]");
    private final By DELETE_BUTTONS = By.xpath("//input[@type='submit'][@value='Delete']");

    /**
     * Method to navigate directly to the shopping basket page if required, without having to
     * open the homepage and click the Basket button
     */
    public void navigateToShoppingBasketPage() {
        commonUtils.navigateToURL(Urls.SHOPPING_BASKET_PAGE.getUrl());
    }

    public String getShoppingCartMessage() {
        return commonUtils.getElementText(SHOPPING_BASKET_MESSAGE).trim();
    }

    public void deleteItemInBasket() throws InterruptedException {
        //for some reason, if we do not refresh below and wait, it will not remove the last item in the basket :(
        //I am not sure why this started failing from before, the delete button is there and clickable (tested via code)
        // but it doesn't think it is
        commonUtils.getDriver().navigate().refresh();
        commonUtils.waitForElementToBeVisible(DELETE_BUTTONS);
        var deleteItem = commonUtils.getElement(DELETE_BUTTONS);
        commonUtils.waitForElementToBeClickable(DELETE_BUTTONS);
        deleteItem.click();
        Thread.sleep(1000);
    }

    /**
     * Removes all items in basket iteratively, waiting for the basket cart count to decrease each time, to give the
     * website time to remove the items and verify we are in the correct state.
     * Because the page seems to refresh after one item is removed, getting all webelements and clicking on them
     * in a loop fails, even after trying different waits.
     *
     * Obviously in real life we would do this via the DB to save such hacky code
     */
    public void removeAllItemsFromBasketIfPresent() throws InterruptedException {
        TopBanner topBanner = new TopBanner(commonUtils);
        ShoppingBasketPage shoppingBasket = new ShoppingBasketPage(commonUtils);

        topBanner.selectShoppingCartIcon();  // brings up list of items in basket

        var items = Integer.parseInt(topBanner.getNumberOfItemsListedInShoppingCartIcon());

        while(items != 0){
            log.info("Removing item/s from basket:" + "Item: " + items);
            shoppingBasket.deleteItemInBasket();
            items--;
            commonUtils.wait = new WebDriverWait(commonUtils.getDriver(), Duration.ofSeconds(10));
            commonUtils.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("nav-cart-count"), String.valueOf(items)));
        }
    }

    public String getHeaderOfItemInBasket() {
        return commonUtils.getElementText(HEADER_OF_ITEM_IN_BASKET).trim();
    }

    public ArrayList<String> getHeadersOfAllItemsInBasket() {
        var headerElements = commonUtils.getElements(HEADER_OF_ITEM_IN_BASKET);
        ArrayList<String> itemHeaders = new ArrayList<>();

        for (WebElement headers : headerElements) {
            String headerText = headers.getText().trim();
            itemHeaders.add(headerText);
        }
        return itemHeaders;
    }
}