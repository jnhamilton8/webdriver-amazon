package jnhamilton.selenium.pageobjects;

import jnhamilton.selenium.testdata.LoadProduct;
import jnhamilton.selenium.utils.CommonUtils;
import jnhamilton.selenium.utils.Urls;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;

import java.util.HashMap;

@Slf4j
public class ProductPage {

    private final CommonUtils commonUtils;
    LoadProduct products = new LoadProduct();

    public ProductPage(CommonUtils commonUtils) {
        this.commonUtils = commonUtils;
    }

    private final By PRODUCT_TITLE = By.id("productTitle");
    private final By AUTHOR = By.cssSelector(".a-link-normal.contributorNameID");
    private final By EDITION = By.xpath("//h1[@id = 'title']/span[@class = 'a-size-medium a-color-secondary a-text-normal'][1]");
    private final By PRICE = By.id("price");
    private final By ADD_TO_BASKET = By.xpath("//span[@id='submit.add-to-cart']");
    private final By ADDED_TO_BASKET_CONFIRMATION_MESSAGE = By.xpath("//span[contains(text(),'Added to Basket')]");

    public String getProductTitle() {
        return commonUtils.getElementText(PRODUCT_TITLE);
    }

    public String getAuthor() {
        return commonUtils.getElementText(AUTHOR);
    }

    public String getEdition() {
        return commonUtils.getElementText(EDITION);
    }

    public String getPrice() {
        return commonUtils.getElementText(PRICE);
    }

    /**
     * Goes directly to the book product page, creating the URL using the base url,
     * products url and the ID of the product obtained from the json file
     * Saves having to search etc. for item first
     *
     * @param product the name of the book product
     */
    public void navigateToBookProductPage(String product) {
        var url = Urls.BASE_URL.getUrl() + Urls.PRODUCTS_URL.getUrl() + products.getBookProductID(product);
        commonUtils.navigateToURL(url);
        log.info("BOOK_PAGE: navigated to the book product page: " + url);
        log.info("The book used was: " + products.getProductJsonArray(product));
    }

    /**
     * Helper method to quickly add an item to the basket by navigating directly to the book product page
     * and clicking the Add to Basket button.
     *
     * @param product the name of the book product
     */
    public void addItemToBasket(String product) {
        navigateToBookProductPage(product);
        clickAddToBasket();
        commonUtils.waitForElementToBeVisible(ADDED_TO_BASKET_CONFIRMATION_MESSAGE);
    }

    /**
     * Takes the user to the Basket Review page
     */

    public void clickAddToBasket() {
        commonUtils.click(ADD_TO_BASKET);
    }

    /**
     * Retrieves all the actual product values for later comparison
     *
     * @param product the name of the book product
     * @return HashMap expectedProductDetails
     */
    public HashMap<String, String> getExpectedProductDetails(String product) {
        navigateToBookProductPage(product);

        var actualPrice = this.getPrice();
        var actualAuthor = this.getAuthor();
        var actualEdition = this.getEdition();
        var actualTitle = this.getProductTitle();

        HashMap<String, String> expectedProductDetails = new HashMap<>();
        expectedProductDetails.put("price", actualPrice);
        expectedProductDetails.put("edition", actualEdition);
        expectedProductDetails.put("author", actualAuthor);
        expectedProductDetails.put("title", actualTitle);

        log.info("Retrieved the following product details: " + expectedProductDetails);

        return expectedProductDetails;
    }
}