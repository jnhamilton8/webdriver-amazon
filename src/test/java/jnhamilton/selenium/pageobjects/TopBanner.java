package jnhamilton.selenium.pageobjects;

import jnhamilton.selenium.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * To use to Top Banner, to to the Home page first or use DriverManager.get to go directly to the BASE_URL
 */
@Slf4j
public class TopBanner {

    private final CommonUtils commonUtils;

    public TopBanner(CommonUtils commonUtils) {
        this.commonUtils = commonUtils;
    }

    private static final By SIGN_IN_LOCATOR = By.xpath("//span[contains(., 'Hello, sign in')]");
    private static final By BASKET_CART_ICON = By.id("nav-cart");
    private static final By BASKET_CART_COUNT = By.id("nav-cart-count");

    public void navigateToSignInPageFromTopBanner() {
        commonUtils.click(SIGN_IN_LOCATOR);
        commonUtils.wait.until(ExpectedConditions.titleIs("Amazon Sign In"));
    }

    public void selectShoppingCartIcon() {
        commonUtils.waitForElementToBeVisible(BASKET_CART_ICON);
        commonUtils.click(BASKET_CART_ICON);
        commonUtils.wait.until(ExpectedConditions.titleIs("Amazon.co.uk Shopping Basket"));
    }

    public String getNumberOfItemsListedInShoppingCartIcon() {
        return commonUtils.getElementText(BASKET_CART_COUNT);
    }
}