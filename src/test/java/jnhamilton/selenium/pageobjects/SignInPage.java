package jnhamilton.selenium.pageobjects;

import jnhamilton.selenium.utils.CommonUtils;
import jnhamilton.selenium.utils.TestBase;
import jnhamilton.selenium.utils.Urls;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@Slf4j
public class SignInPage extends TestBase {

    CommonUtils commonUtils;

    public SignInPage(CommonUtils commonUtils) {
        this.commonUtils = commonUtils;
    }

    private static final By SIGN_IN_NAME_LOCATOR = By.id("ap_email");
    private static final By SIGN_IN_PASSWORD_LOCATOR = By.id("ap_password");
    private static final By CONTINUE_BUTTON = By.id("continue");
    private static final By SIGN_IN_BUTTON = By.id("signInSubmit");
    private static final By AUTH_WARNING = By.id("auth-warning-message-box");
    private static final By PASSWORD_RESET_REQUIRED = By.xpath("//div[@id='authportal-main-section']//h2");

    /**
     * Provides the ability to navigate directly to the sign in page using the URL
     * to save time in tests.  There is a method in TopBanner to navigate to the same page by
     * clicking the Sign In button if required
     */
    public void navigateDirectlyToSignInPage() {
        commonUtils.navigateToURL(Urls.SIGN_IN_PAGE.getUrl());
    }

    public void enterUserName(String userName) {
        log.info("Entering username: ({})", userName);
        commonUtils.waitForElementToBeVisible(SIGN_IN_NAME_LOCATOR);
        commonUtils.sendKeys(SIGN_IN_NAME_LOCATOR, userName);
        clickContinueButton();
    }

    private void enterPassword(String password) {
        log.info("Entering password: ({})", password);
        commonUtils.waitForElementToBeVisible(SIGN_IN_PASSWORD_LOCATOR);
        commonUtils.sendKeys(SIGN_IN_PASSWORD_LOCATOR, password);
    }

    private void clickSignInButton() {
        commonUtils.click(SIGN_IN_BUTTON);
    }

    private void clickContinueButton() {
        commonUtils.click(CONTINUE_BUTTON);
    }

    public void submitSignIn(String username, String password) {
        enterUserName(username);
        enterPassword(password);
        log.info("Logging in as user ({})", username);
        clickSignInButton();
    }

    public void navigateDirectlyToLoginPageAndLogIn(String username, String password){
        navigateDirectlyToSignInPage();
        submitSignIn(username, password);
    }

    public Boolean getVisibilityOfAuthWarning() {
        commonUtils.waitForElementToBeVisible(AUTH_WARNING);
        var auth = commonUtils.getElement(AUTH_WARNING);
        return auth.isDisplayed();
    }

    public String getTextOfNotificationOnLogIn() {
        commonUtils.waitForElementToBeVisible(PASSWORD_RESET_REQUIRED);
        return commonUtils.getElement(PASSWORD_RESET_REQUIRED).getText();
    }
}