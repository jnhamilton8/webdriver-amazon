package jnhamilton.selenium.pageobjects;

import jnhamilton.selenium.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;

/**
 * We cannot easily navigate to the BasketReviewPage directly, hence no direct navigation method
 * You need to have added items to the basket to take you to this page, via ProductPage
 */
@Slf4j
public class BasketReviewPage {

    private final CommonUtils commonUtils;

    public BasketReviewPage(CommonUtils commonUtils) {
        this.commonUtils = commonUtils;
    }

    private final By PRICE = By.xpath("//span[contains(@class,'a-color-price')]");
    private final By BASKET_SUBTOTAL_TEXT = By.xpath("//span[contains(@class,'a-color-price')]");

    public String getCartSubTotal() {
        return commonUtils.getElementText(PRICE);
    }

    public void verifyOnBasketReviewPage() {
        commonUtils.waitForElementToBeVisible(BASKET_SUBTOTAL_TEXT);
    }
}