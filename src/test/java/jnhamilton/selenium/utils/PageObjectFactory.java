package jnhamilton.selenium.utils;

import jnhamilton.selenium.pageobjects.*;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class PageObjectFactory extends TestBase {

    private final WebDriver driver;
    Map<String, Object> requiredPageObjects = new HashMap<>();
    private BasketReviewPage basketReviewPage;
    private SignInPage signInPage;
    private HomePage homePage;
    private ProductPage productPage;
    private ShoppingBasketPage shoppingBasketPage;
    private TopBanner topBanner;
    private CommonUtils commonUtils;

    public PageObjectFactory(WebDriver driver) {
        this.driver = driver;
    }

    public CommonUtils commonUtils() {
        return (CommonUtils) requiredPageObjects.get("commonUtils");
    }
    public BasketReviewPage basketReviewPage() { return (BasketReviewPage) requiredPageObjects.get("basketReviewPage");}
    public SignInPage signInPage() {
        return (SignInPage) requiredPageObjects.get("signInPage");
    }
    public HomePage homePage() {
        return (HomePage) requiredPageObjects.get("homePage");
    }
    public ProductPage productPage() {
        return (ProductPage) requiredPageObjects.get("productPage");
    }
    public TopBanner topBanner() {
        return (TopBanner) requiredPageObjects.get("topBanner");
    }
    public ShoppingBasketPage shoppingBasketPage() {
        return (ShoppingBasketPage) requiredPageObjects.get("shoppingBasketPage");
    }

    public void setupPageObjects(List<String> pageObjectsList) {
        commonUtils = new CommonUtils(driver);
        requiredPageObjects.put("commonUtils", commonUtils);

        for (String pageObject : pageObjectsList) {
            switch (pageObject) {
                case "basketReviewPage":
                    basketReviewPage = new BasketReviewPage(commonUtils);
                    requiredPageObjects.put("basketReviewPage", basketReviewPage);
                    break;
                case "signInPage":
                    signInPage = new SignInPage(commonUtils);
                    requiredPageObjects.put("signInPage", signInPage);
                    break;
                case "homePage":
                    homePage = new HomePage(commonUtils);
                    requiredPageObjects.put("homePage", homePage);
                    break;
                case "productPage":
                    productPage = new ProductPage(commonUtils);
                    requiredPageObjects.put("productPage", productPage);
                    break;
                case "shoppingBasketPage":
                    shoppingBasketPage = new ShoppingBasketPage(commonUtils);
                    requiredPageObjects.put("shoppingBasketPage", shoppingBasketPage);
                    break;
                case "topBanner":
                    topBanner = new TopBanner(commonUtils);
                    requiredPageObjects.put("topBanner", topBanner);
                    break;
                default:
                    log.error("No such page object");
                    throw new IllegalArgumentException("No such page object: " + pageObject + " please check");
            }
        }
    }
}
