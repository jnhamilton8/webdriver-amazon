package jnhamilton.selenium.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.devtools.DevTools;

import org.openqa.selenium.devtools.DevToolsException;
import org.openqa.selenium.devtools.v104.log.Log;
import org.openqa.selenium.devtools.v104.network.Network;
import org.openqa.selenium.devtools.v104.network.model.RequestId;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.WebDriverListener;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Slf4j
public class DriverFactory {

    private WebDriver driver;
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss:SSS");
    private LocalDateTime now;

    public WebDriver driverInit(String browserName) {
        switch (browserName.toLowerCase()) {
            case "chrome":
                createChromeDriver();
                break;
            case "firefox":
                createFireFoxDriver();
                break;
            case "edge":
                createEdgeDriver();
                break;
            default:
                createChromeDriver();
                break;
        }

        WebDriverListener listener = new WebEventListener();

        return new EventFiringDecorator<>(listener).decorate(driver);
    }

    private void createChromeDriver() {
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-plugins");
        options.addArguments("disable-extensions");
        options.addArguments("test-type");

        driver = new ChromeDriver();

        setUpConsoleAndNetworkLoggingForChrome();
    }

    private void createFireFoxDriver() {
        WebDriverManager.firefoxdriver().setup();

        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("disable-plugins");
        options.addArguments("disable-extensions");

        driver = new FirefoxDriver();
    }

    private void createEdgeDriver() {
        WebDriverManager.edgedriver().setup();

        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);

        driver = new EdgeDriver(edgeOptions);
    }

    private void setUpConsoleAndNetworkLoggingForChrome() {
        try {
            OutputStream consoleLogfile = new FileOutputStream(String.format("./logs/console-%s.log", Instant.now().getEpochSecond()), true);
            OutputStream networkLogfile = new FileOutputStream(String.format("./logs/network-%s.log", Instant.now().getEpochSecond()), true);
            PrintStream printConsoleLog = new PrintStream(consoleLogfile);
            PrintStream printNetworkLog = new PrintStream(networkLogfile);

            DevTools devTools = ((ChromeDriver) driver).getDevTools();
            devTools.createSessionIfThereIsNotOne();

            devTools.send(Log.enable());

            devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));
            devTools.send(Network.clearBrowserCache());
            devTools.send(Network.setCacheDisabled(true));

            final RequestId[] requestIds = new RequestId[1];

            devTools.addListener(Log.entryAdded(),
                    logEntry -> {
                        now = LocalDateTime.now();
                        printConsoleLog.append(now.format(dtf)).append(": ").append(logEntry.getText());
                        printConsoleLog.append(System.getProperty("line.separator"));
                    });

            devTools.addListener(Network.responseReceived(), logEntry -> {
                requestIds[0] = logEntry.getRequestId();
                now = LocalDateTime.now();

                if (logEntry.getType().toString().equals("XHR")) { //we only care about XHR requests, not image, script etc
                    try {
                        printNetworkLog.append(
                                now.format(dtf)).append(": ").append("RESPONSE URL: ").append(logEntry.getResponse().getUrl())
                                .append(System.getProperty("line.separator"))
                                .append(now.format(dtf)).append(": ").append("RESPONSE STATUS: ").append("(").append(logEntry.getResponse().getStatus().toString()).append(")")
                                .append(System.getProperty("line.separator"))
                                .append(now.format(dtf)).append(": ").append("RESPONSE BODY: ").append(devTools.send(Network.getResponseBody(requestIds[0])).getBody())
                                .append(System.getProperty("line.separator"));
                    } catch (DevToolsException dev) {
                        log.info("The request id of {} did not have a corresponding request body", requestIds[0]);
                    }
                }
            });

            devTools.addListener(Network.requestWillBeSent(), logEntry -> {
                requestIds[0] = logEntry.getRequestId();
                now = LocalDateTime.now();
                printNetworkLog.append(
                        now.format(dtf)).append(": ").append(logEntry.getRequest().getMethod())
                        .append(" REQUEST URL: ").append(logEntry.getRequest().getUrl())
                        .append(System.getProperty("line.separator"));
            });

        } catch (FileNotFoundException ex) {
            log.error("Oh no! We couldn't write the logs!");
        }
    }
}
