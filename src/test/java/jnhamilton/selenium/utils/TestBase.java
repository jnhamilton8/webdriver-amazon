package jnhamilton.selenium.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;

@Listeners({TestListenerAllure.class})
@Slf4j
@Getter
@Setter
@NoArgsConstructor(force = true)
public class TestBase {

    public WebDriver driver;

    public void initDriver(String browserName) {
        DriverFactory driverFactory = new DriverFactory();
        setDriver(driverFactory.driverInit(browserName));
    }

    @Parameters({"browserName"})
    @BeforeClass
    public void beforeClass(String browserName){
        initDriver(browserName);
        driver.manage().window().maximize();
    }

    @AfterClass
    public void afterClass() {
        if (getDriver() != null) {
            getDriver().quit();
            setDriver(null);
        }
    }
}
